##!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# --------------------------------------------------------------------------------
# Script:       Snapora
# Version:      0.x.x
# Author:       DeaDSouL (Mubarak Alrashidi)
# URL:          https://unix.cafe/
# GitLab:       https://gitlab.com/DeaDSouL/snapora
# Mastodon:     https://fosstodon.org/@DeaDSouL
# Twitter:      https://twitter.com/_DeaDSouL_
# License:      GPLv3
# --------------------------------------------------------------------------------
#           Enjoy the features of combining Fedora, BtrFS and Snapper
# --------------------------------------------------------------------------------

# Vim ? #{{{
#   It's recommended to use `vim` to edit this script. As it's also recommended 
#       to have 'set modeline' and 'set encoding=utf-8' in your $HOME/.vimrc
#
#   Folding hints:
#       Create fold: visual select what you want, then hit: zf
#       Delete folde: go to folded line then hit: zd
#       zo: open fold
#       zc: close fold
#       za: toggle fold
#       zO, zC, zA: same as above, but for all
#       zr: open all folds
#       zm: close all folds
# #}}}
# --------------------------------------------------------------------------------
# Sub-volumes tree: #{{{
# @fedora
# ├── @homefs <-- (mounted as: /home)
# │   ├── user1
# │   │   ├── .cache
# │   │   ├── .local <--------------- (is a directory)
# │   │   │   └── share <------------ (is a directory)
# │   │   │       ├── containers
# │   │   │       └── gnome-boxes <-- (is a directory)
# │   │   │           └── images <--- (mounted with option: nodatacow)
# │   │   ├── .var
# │   │   └── Downloads
# │   ├── user2
# │   │   └── ((Same as above.))
# │   └── userN
# │       └── ((Same as above.))
# ├── @rootfs <-- (mounted as: /)
# │   ├── opt
# │   ├── root
# │   ├── srv
# │   ├── tmp
# │   ├── usr <----------------- (is a directory)
# │   │   └── local
# │   └── var <----------------- (is a directory)
# │       ├── cache
# │       ├── crash
# │       ├── lib <------------- (is a directory)
# │       │   ├── containers
# │       │   ├── libvirt <----- (is a directory)
# │       │   │   └── images <-- (mounted with option: nodatacow)
# │       │   ├── mariadb <----- (mounted with option: nodatacow)
# │       │   ├── mysql <------- (mounted with option: nodatacow)
# │       │   └── pgsql <------- (mounted with option: nodatacow)
# │       ├── log
# │       ├── opt
# │       ├── spool
# │       └── tmp
# └── @snapshots
#     ├── @home-root <--- (mounted as: /root/.snapshots)
#     ├── @home-user1 <-- (mounted as: /home/user1/.snapshots)
#     ├── @home-user2 <-- (mounted as: /home/user2/.snapshots)
#     ├── @home-userN <-- (mounted as: /home/userN/.snapshots)
#     ├── @rootfs <------ (mounted as: /.snapshots)
#     └── @usr-local <--- (mounted as: /usr/local/.snapshots)
# #}}}
# --------------------------------------------------------------------------------
# @TODO: #{{{
#   01) Take a snapshot of /home & /root, before we proceed.
#   02) Put a warning, that this script:
#       - does not support the encrypted systems yet.
#           - maybe make it exit once it detects an encrypted disk?
#       - works better if we have / & /home as subvolumes.
#       - works only if our filesystem is BtrFS.
#       - won't add a mount entry for swap in /etc/fstab.
#       - ...
#   03) Make sure user is root, or has its powers.
#   04) Make sure the partition's filesystem is BtrFS.
#   05)     DONE - Create .snapshots directories for /, /root, /home/[USER], /usr/local.
#   06)     DONE - Identify the disk whether or not it's SSD, `cat /sys/block/[DISK]/queue/rotational`
#   07) Make homefs flixble (means we could proceed even if we don't have homefs subvolume)
#   08) Make a dry-run option to show what will happen.
#   09) Make an undo option, to revert what this script did.
#   10) Make
# #}}}
# --------------------------------------------------------------------------------

# Settings #{{{
# Partitions
_UEFI_PARTITION='/dev/nvme0n1p1'
_BOOT_PARTITION='/dev/nvme0n1p2'
_BTRFS_PARTITION='/dev/nvme0n1p3'

# where the $_BTRFS_PARTITION is going to be mounted temporarly
_BTRFS_MNT='/mnt/btrfs'

_SNAPPER_TEMPLATE='/usr/share/snapper/config-templates/default'

# --------------------------------------------------------------------------------

# Most of people, don't really need to alter anything else below this line

# --------------------------------------------------------------------------------

# in case, you're going to have multiple distros on the same disk,
# you can group them by the distro name for example.
# if it was specified, it must ends with a trailing slash.
# if you don't want to use one subvolume for each distro, just leave it empty.
_SV_DISTRO='@fedora/'

# the current 'root filesystem' subvolume name that has been created by fedora anaconda installer
_SV_ROOTFS_DEFAULT='root'

# the 'root filesystem' subvolume name that's going to hold all OS root directories
_SV_ROOTFS='@rootfs'

# the current 'home' subvolume name that has been created by fedora anaconda installer
_SV_HOMEFS_DEFAULT='home'

# the home subvolume name that's going to hold users home directories
_SV_HOMEFS='@homefs'

_SV_SNAPSHOTS='@snapshots'

# BtrFS mount options
# NOTE: do NOT add 'ssd', as it will be added automatically if needed
_MNT_OPTS='defaults,noatime,compress=zstd:1'

# username(s)
_HOME_USERS=($(ls -d /home/*/ | xargs basename))

# an array that holds all subvolumes that need to be created
declare -a _DIRS2SV=(
    # Users subvolumes
    # You may add/remove any of the following depends on your needs
    # PS: it's recommended to keep the:
    #   "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_HOMEFS}/${_user}/.local/share/gnome-boxes/images"
    # as it needs the 'nodatacow' mount option, if you ever decide to use gnome-boxes
    $(for _user in "${_HOME_USERS[@]}"; do
        echo "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_HOMEFS}/${_user}";
        echo "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_HOMEFS}/${_user}/Downloads";
        echo "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_HOMEFS}/${_user}/.cache";
        # We changed the subvolume of '~/.local/share/containers/storage' to be as
        # '~/.local/share/containers', since podman needs to be able to delete
        # '~/.local/share/containers/storage' when `podmand system reset` is issued
        echo "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_HOMEFS}/${_user}/.local/share/containers";
        echo "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_HOMEFS}/${_user}/.local/share/gnome-boxes/images";
        echo "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_HOMEFS}/${_user}/.var";
    done)

    # Subvolumes to be excluded from @rootfs's snapshots
    # PS: it's recommended to keep the:
    #   "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/libvirt/images"
    #   "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/mariadb"
    #   "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/mysql"
    #   "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/pgsql"
    # as they need the 'nodatacow' mount option, if you ever decide to use their packages
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/tmp"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/opt"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/srv"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/opt"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/tmp"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/spool"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/crash"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/cache"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/log"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/containers"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/libvirt/images"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/mariadb"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/mysql"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/var/lib/pgsql"

    # Subvolumes to have their own snapshots
    # @TODO: find a solution for /etc, to be included in @rootfs snapshots, as well as it gets to have its own snapshots.
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/root"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/usr/local"

    # Snapshots Subvolumes
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_SNAPSHOTS}"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_SNAPSHOTS}/@rootfs"
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_SNAPSHOTS}/@home-root"
    $(for _user in "${_HOME_USERS[@]}"; do echo "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_SNAPSHOTS}/@home-${_user}"; done)
    "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_SNAPSHOTS}/@usr-local"
    #"${_BTRFS_MNT}/${_SV_DISTRO}${_SV_SNAPSHOTS}/@etc"
)

# what subvolumes that require the 'nodatacow' mount option?
# DO NOT delete any of the following elements, even if you do not
# need any of them
declare -a _SV_NOCOW=(
    $(for _user in "${_HOME_USERS[@]}"; do echo "/home/${_user}/.local/share/gnome-boxes/images"; done)
    "/var/lib/libvirt/images"
    "/var/lib/mariadb"
    "/var/lib/mysql"
    "/var/lib/pgsql"
)

# what subvolumes should have its own snapper config
# in order to have its own snapshots
# [/path/to/mounted/subvolume]='the-config-name'
declare -A _SNAP_CONFIGS=(
    ["/"]="root"
    ["/usr/local"]="usr-local"
    ["/root"]="home-root"
)
for _user in "${_HOME_USERS[@]}"; do
    _SNAP_CONFIGS["/home/${_user}"]="home-${_user}"
done

# Snapper settings that can be applied to root & user
declare -A _SNAP_SETTINGS_BOTH=(
    ["FSTYPE"]="btrfs"
    ["QGROUP"]=""
    ["SPACE_LIMIT"]="0.5"
    ["FREE_LIMIT"]="0.2"
    ["ALLOW_USERS"]=""
    ["ALLOW_GROUPS"]=""
    ["BACKGROUND_COMPARISON"]="yes"
    ["NUMBER_CLEANUP"]="yes"
    ["NUMBER_MIN_AGE"]="1800"
    ["NUMBER_LIMIT"]="50"
    ["NUMBER_LIMIT_IMPORTANT"]="10"
    ["TIMELINE_CREATE"]="yes"
    ["TIMELINE_CLEANUP"]="yes"
    ["EMPTY_PRE_POST_CLEANUP"]="yes"
    ["EMPTY_PRE_POST_MIN_AGE"]="1800"
)

# This configuration keeps one snapshot for each of the previous
# 24 hours, 10 days, and 3 weeks. We could retain snapshots for months
# and years, but for normal desktop’s root filesystem this just isn’t
# unnecessary. Refer to Tuning Periodic Snapshotting:
# https://github.com/kdave/btrfsmaintenance#tuning-periodic-snapshotting
# for good rules of thumb.
declare -A _SNAP_SETTINGS_SYS=(
    ["SYNC_ACL"]="no"
    ["TIMELINE_MIN_AGE"]="1800"
    ["TIMELINE_LIMIT_HOURLY"]="24"
    ["TIMELINE_LIMIT_DAILY"]="10"
    ["TIMELINE_LIMIT_WEEKLY"]="3"
    ["TIMELINE_LIMIT_MONTHLY"]="0"
    ["TIMELINE_LIMIT_YEARLY"]="0"
)

# This configuration keeps one snapshot for each of the previous
# 48 hours, 14 days, 8 weeks, 12 months, and 2 years. This is quite extensive,
# but for preserving critical data in a user’s home directory it’s sensible.
declare -A _SNAP_SETTINGS_USER=(
    ["SYNC_ACL"]="yes"
    ["TIMELINE_MIN_AGE"]="1800"
    ["TIMELINE_LIMIT_HOURLY"]="48"
    ["TIMELINE_LIMIT_DAILY"]="14"
    ["TIMELINE_LIMIT_WEEKLY"]="8"
    ["TIMELINE_LIMIT_MONTHLY"]="12"
    ["TIMELINE_LIMIT_YEARLY"]="2"
)
#}}}

# --------------------------------------------------------------------------------

# Needed variables #{{{
_DATETIME=$(date +'%Y%m%d%H%M%S')
_FSTAB_DT=$(date +'%a %b %d %H:%M:%S %Y')
_COMMANDS_OUTPUT="/snapora.log-${_DATETIME}"
_ESCHE='\e'         # Escape character<200c><200b> for 'echo' in colors
# Colors. Usage `echo -e "${_RED}some text"
_NC='\e[0m'         # No Color: Text Reset
# 40 = Black background. 0 = No background.
_WHITE='\e[40;37m'  # White
_RED='\e[40;31m'    # Red
_BLUE='\e[40;34m'   # Blue
_GREEN='\e[40;32m'  # Green
# Needed Arrays
REQUIRED_PACKAGES=(grubby snapper python3-dnf-plugin-snapper)
declare -a GUESSED_SNAPPER_CONFIG_TEMPLATES=()
#}}}

# Needed helper functions #{{{
# cleanup
function _cleanup() { #{{{
    # maybe we should remove all empty directories end with "*.def" ??
    echo -e "\nCleaning up,.."
} #}}}

# exit
function _exit() { #{{{
    if mount | grep "${_BTRFS_MNT}" >/dev/null 2>&1; then
        umount -v "${_BTRFS_MNT}"
        _log $? "Un-mounting '${_BTRFS_MNT}'." "Could not un-mount '${_BTRFS_MNT}'!"
    fi
    if ! mount | grep "${_BTRFS_MNT}" >/dev/null 2>&1; then
        if [ -d "${_BTRFS_MNT}" ]; then
            rmdir -v "${_BTRFS_MNT}" &>>"${_COMMANDS_OUTPUT}"
            _log $? "Removing directory: '${_BTRFS_MNT}'." "Could not remove directory: '${_BTRFS_MNT}'!"
        fi
    fi

    _COMMANDS_OUTPUT_OLD="${_COMMANDS_OUTPUT}"
    _COMMANDS_OUTPUT="/var/log${_COMMANDS_OUTPUT}"
    [ -e "${_COMMANDS_OUTPUT_OLD}" ] && mv "${_COMMANDS_OUTPUT_OLD}" "${_COMMANDS_OUTPUT}"

    echo -e "\n----------------------------------------------------------------------"
    [ "$1" == 0 ] && echo "It looks like everything has been successfully done."
    [ "$1" != 0 ] && echo "It looks like something has been failed."
    echo "You can check the script logs at: ${_COMMANDS_OUTPUT}"
    echo "----------------------------------------------------------------------"

    if [ "$1" != 0 ]; then exit 1;
    else exit 0; fi
} #}}}

# abort
function _abort() { #{{{
    echo -e "$1\nAborting,.."
    _exit 1
} #}}}

# log status #{{{
#   PARAMETER 1: the status of the previousely executed command. ($?)
#   PARAMETER 2: headline.
#   PARAMETER 3: to abort if it fails, what massage should we print just before aborting.
#}}}
function _log() { #{{{
    if [ "$1" == 0 ]; then
        echo -e "${_BLACK_BG}${_BLUE}[${_GREEN} OK ${_BLUE}]${_WHITE} : $2${_NC}"
        echo "[ OK ] : $2" >> "${_COMMANDS_OUTPUT}"
    else
        echo -e "${_BLUE}[${_RED}FAIL${_BLUE}]${_WHITE} : $2${_NC}"
        echo "[FAIL] : $2" >> "${_COMMANDS_OUTPUT}"
        # if the third parameter was provided, it means we should exit if the status was anything but 0
        if [ ! -z "$3" ]; then _abort "$3"; fi
    fi
} #}}}

# create subvolume
function _mksv() { #{{{
    if [ ! -e "${1}" ]; then
        btrfs subvolume create "${1}" &>>"${_COMMANDS_OUTPUT}"
        _log $? "Creating Subvolume: '${1}'." "Could not create Subvolume: '${1}'!"
    else
        _log $? "Creating Subvolume: '${1}'." "'${1}' Already exists!"
    fi
} #}}}

# creating parent directories, then create the given subvolume
function _mksv_wpdirs() { #{{{
    # $1 -> ex: /mnt/btrfs/@fedora/@rootfs/var/opt
    _dir2sv=$(dirname "$1")
    # $2 -> ex: /var/opt
    _svpath=$(dirname "$2")

    # if it is a /home sub-directory
    if [[ "${2}" == "/home/"* ]]; then _user=$(echo "${2}" | cut -d'/' -f3)
    else _user='root'; fi

    if [ ! -e "${_svpath}" ]; then
        declare -a _created_dirs

        while [ ! -e "${_svpath}" ]; do
            _created_dirs+=("${_svpath}")
            _svpath=$(dirname "${_svpath}")
        done

        _reserve_dirname "${_svpath}"

        mkdir -vp "${_dir2sv}" &>>"${_COMMANDS_OUTPUT}"
        _log $? "Making directory: '${_dir2sv}'." "Could not make directory: '${_dir2sv}'!"

        for _created_dir in "${_created_dirs[@]}"; do
            _restore_con_own "${_created_dir}" "${_user}"
        done
    fi

    _mksv "${2}"
    _restore_con_own "${2}" "${_user}"
} #}}}

# move subvolumes
function _mvsv() { #{{{
    _src_="$1"
    _dst_="$2"
    if [ "${_src_}" == "${_dst_}" ]; then
        echo "src and dst both equal to: '${_src_}'!" &>>"${_COMMANDS_OUTPUT}"
        _log 1 "Moving '${_src_}' to '${_dst_}'."
        return 1
    fi
    if [ -e "${_src_}" -a ! -e "${_dst_}" ]; then
        mv -v "${_src_}" "${_dst_}" &>>"${_COMMANDS_OUTPUT}"
        _log $? "Moving '${_src_}' to '${_dst_}'." "Could not move '${_src_}' to '${_dst_}'!"
    else
        _log $? "Moving '${_src_}' to '${_dst_}'." "'${_src_}' does not exist, AND|OR '${_dst_}' does exist!"
    fi
} #}}}

# reserve the directory name
function _reserve_dirname() { #{{{
    # what if it exists but it isn't a directory?!
    if [ -e "${1}" -a ! -d "${1}" ]; then
        # back it up
        mv -v "${1}"{,.def} &>>"${_COMMANDS_OUTPUT}"
        _log $? "Renaming '${1}' to '${1}.def'." "Could not rename '${1}' to '${1}.def'!"
    fi
} #}}}

# restore context & ownership by given user
function _restore_con_own() { #{{{
    # $1: _svpath ----> ex: /var/log
    # $2: _username --> ex: deadsoul
    restorecon -vv "${1}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Restoring the security context (SELinux) of '${1}'."
    chown -v "${2}:${2}" "${1}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Restoring the ownership of '${1}'."
} #}}}

# does the given path need 'nodatacow' mount option?
function _sv_needs_cow() { #{{{
    # is ${1} in ${_SV_NOCOW} array
    # note: the spaces between '"' are needed to match the exact value
    if [[ " ${_SV_NOCOW[@]} " =~ " ${1} " ]]; then return 0
    else return 1; fi
} #}}}

# make a backup of a given file
function _bkp_file() { #{{{
    _fname="$1"
    if [ -z "${_fname}" ]; then
        echo "\$_fname equals to '${_fname}' which is empty!" &>>"${_COMMANDS_OUTPUT}"
        _log 1 "Backing up '${_fname}'."
        return 1
    fi

    if [ ! -e "${_fname}" ]; then
        echo "'${_fname}' does not exist!" &>>"${_COMMANDS_OUTPUT}"
        _log 1 "Backing up '${_fname}'."
        return 1
    fi

    _bkpname="${_fname}.bkp-${_DATETIME}"
    [ -e "${_bkpname}" ] && _bkpname+="-"$(date +'%N')

    mv -v "${_fname}" "${_bkpname}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Renaming '${_fname}' to '${_bkpname}'." "Could not rename '${_fname}' to '${_bkpname}'!"

    cp -Zapv "${_bkpname}" "${_fname}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Copying '${_bkpname}' to '${_fname}'." "Could not copy '${_bkpname}' to '${_fname}'!"

    # Make sure we restore the correct ownership & group
    chown -v --reference="${_bkpname}" "${_fname}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Restoring the ownership of '${_fname}'." "Could not restore the ownership of '${_fname}'!"

    # Make sure we restore the correct octal permission
    chmod -v $(stat -c "%a" "${_bkpname}") "${_fname}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Restoring the octal permission of '${_fname}'." "Could not restore the octal permission of '${_fname}'!"

    # Make sure we restore the correct security context (SELinux)
    chcon -v --reference="${_bkpname}" "${_fname}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Restoring the security context (SELinux) of '${_fname}'." "Could not restore the security context (SELinux) of '${_fname}'!"
} #}}}

# add setting values to snapper config file
function _chfile_values() { #{{{
    # $1 : associative array (['variable_name']='value')
    # $2 : path to snapper config
    declare -n arr=$1
    [ ! -f "${2}" ] && return 1
    local ret=0
    for _var in "${!arr[@]}"; do
        if sed -i 's%'"${_var}"'=".*"%'"${_var}"'="'"${arr[${_var}]}"'"%' "${2}"; then
            _log 0 "Changing the value of '${_var}' to '${arr[${_var}]}' in '${2}'." "Could not change the value of '${_var}' to '${arr[${_var}]}' in '${2}'!"
        else
            _log 1 "Changing the value of '${_var}' to '${arr[${_var}]}' in '${2}'." "Could not change the value of '${_var}' to '${arr[${_var}]}' in '${2}'!"
            ret=1
        fi
    done
    if [ "${ret}" == 0 ]; then return 0;
    else return 1; fi
}
#}}}

# check the existence of package(s)
function check_packages_existence() { #{{{
    # $1..N packages name
    local ret=0

    PKGS=$@
    for pkg in ${PKGS[@]}; do
        ((checks++))
        if rpm -qa --queryformat='%{NAME}\n' | grep -E "^$pkg$" >/dev/null 2>&1; then
            _log 0 "Checking the existence of: '${pkg}'"
            ((successes++))
        else
            _log 1 "Checking the existence of: '${pkg}'"
            ((fails++))
            ret=1
        fi
    done
    unset pkg PKGS

    if [ "${ret}" == 0 ]; then return 0;
    else return 1; fi
}
#}}}

# guess partitions
function guess_partition() { #{{{
    # $1 : one of: boot|efi|rootfs
    case "$1" in
        boot)   mount | grep ' /boot ' | awk '{print $1}'       ;;
        efi)    mount | grep ' /boot/efi ' | awk '{print $1}'   ;;
        rootfs) mount | grep ' / ' | awk '{print $1}'           ;;
        *)      return 1                                        ;;
    esac
    return 0
}
#}}}

# check partition
function check_partition() { #{{{
    # $1 the value
    # $2 the variable name with '$'
    local ret=0
    local suggestion=''

    case "$2" in
        '$_UEFI_PARTITION')
            mountpoint=' /boot/efi '
            guessed_partition=$(guess_partition efi)
            [ "$guessed_partition" == "$_UEFI_PARTITION" ] && guessed_partition=''
            ;;
        '$_BOOT_PARTITION')
            mountpoint=' /boot '
            guessed_partition=$(guess_partition boot)
            [ "$guessed_partition" == "$_BOOT_PARTITION" ] && guessed_partition=''
            ;;
        '$_BTRFS_PARTITION')
            mountpoint=' / '
            guessed_partition=$(guess_partition rootfs)
            [ "$guessed_partition" == "$_BTRFS_PARTITION" ] && guessed_partition=''
            ;;
        *)
            mountpoint='((INVALID))'
            ;;
    esac

    [ ! -z "$guessed_partition" ] && suggestion="[could it be: $guessed_partition]"

    if [ ! -z "$1" ]; then
        if [[ -e "$1" && -b "$1" ]]; then
            _UUID=$(blkid "$1" -s UUID -o value)
            if grep "$_UUID" /etc/fstab >/dev/null 2>&1; then
                if grep "$_UUID" /etc/fstab | grep "$mountpoint" >/dev/null 2>&1; then
                    _log 0 "Checking: $2='$1'. (probably correct)!"
                else
                    _log 1 "Checking: $2='$1'. (wasn't used for this partition in /etc/fstab)! ${suggestion}"
                fi
            else
                _log 1 "Checking: $2='$1'. (isn't in /etc/fstab)! ${suggestion}"
            fi
        else
            _log 1 "Checking: $2='$1'. (is invalid)! ${suggestion}"
            ret=1
        fi
    else
        _log 1 "Checking: $2='$1'. (is empty)! ${suggestion}"
        ret=1
    fi
    unset _UUID

    ((checks++))
    if [ "${ret}" == 0 ]; then
        ((successes++))
        return 0
    else
        ((fails++))
        return 1
    fi
}
#}}}

# prepare btrfs mount-point
function prepare_btrfs_mountpoint() { #{{{
    local ret=0
    # Make sure "${_BTRFS_MNT}" is not anything but a directory
    if [[ -e "${_BTRFS_MNT}" && ! -d "${_BTRFS_MNT}" ]]; then
        mv -v "${_BTRFS_MNT}" "${_BTRFS_MNT}.bkp-${_DATETIME}" &>>"${_COMMANDS_OUTPUT}"
        s=$?

        # will exit on fail:
        #_log $s "Rename: '${_BTRFS_MNT}' to '${_BTRFS_MNT}.bkp-${_DATETIME}'." "Could not rename directory: '${_BTRFS_MNT}'!"

        # will continue on fail:
        _log $s "Rename: '${_BTRFS_MNT}' to '${_BTRFS_MNT}.bkp-${_DATETIME}'."

        [ $s -ne 0 ] && ret=$s
    fi

    # Make sure "${_BTRFS_MNT}" directory exists
    if [ ! -e "${_BTRFS_MNT}" ]; then
        mkdir -v "${_BTRFS_MNT}" &>>"${_COMMANDS_OUTPUT}"
        s=$?

        # will exit on fail:
        #_log $s "Making directory: '${_BTRFS_MNT}'." "Could not make directory: '${_BTRFS_MNT}'!"

        # will continue on fail:
        _log $s "Making directory: '${_BTRFS_MNT}'."

        [ $s -ne 0 ] && ret=$s
    fi

    if [ "${ret}" == 0 ]; then return 0;
    else return 1; fi
}
#}}}

# check btrfs mount-point
function check_btrfs_mountpoint() { #{{{
    local ret=0

    if prepare_btrfs_mountpoint; then
        if [ -e "${_BTRFS_MNT}" ]; then
            if [ -d "${_BTRFS_MNT}" ]; then
                if [ -w "${_BTRFS_MNT}" ]; then
                    if [ $(find "${_BTRFS_MNT}" -mindepth 1 | wc -l) -eq 0 ]; then
                        _log 0 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'."
                    else
                        if mount | grep "${_BTRFS_MNT}" >/dev/null 2>&1; then
                            if mount | grep "${_BTRFS_MNT}" | grep 'btrfs'  >/dev/null 2>&1; then
                                if mount | grep "${_BTRFS_MNT}" | grep "$_BTRFS_PARTITION"  >/dev/null 2>&1; then
                                    _log 0 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'. (mounted)"
                                else
                                    _log 1 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'. (mounted but it's not '$_BTRFS_PARTITION')"
                                    ret=1
                                fi
                            else
                                _log 1 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'. (mounted but it's not btrfs)"
                                ret=1
                            fi
                        else
                            _log 1 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'. (isn't empty)"
                            ret=1
                        fi
                    fi
                else
                    _log 1 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'. (isn't writable)"
                    ret=1
                fi
            else
                _log 1 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'. (isn't directory)"
                ret=1
            fi
        else
            _log 1 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'. (doesn't exist)"
            ret=1
        fi
    else
        _log 1 "Checking: \$_BTRFS_MNT='${_BTRFS_MNT}'. (isn't prepared)"
        ret=1
    fi

    ((checks++))
    if [ "${ret}" == 0 ]; then
        ((successes++))
        return 0
    else
        ((fails++))
        return 1
    fi
}
#}}}

# check $_DIRS2SV directories states
function check_dir2sv_states() { #{{{
    local ret=0
    for sv in "${_DIRS2SV[@]}"; do
        # ex: @fedora/rootfs/var/opt
        svname="${sv##*${_BTRFS_MNT}/}"
        if [[ "${sv}" =~ .*"${_SV_ROOTFS}" ]]; then
            # ex: /var/opt
            svpath="${sv##*${_SV_DISTRO}${_SV_ROOTFS}}"
        elif [[ "${sv}" =~ "${_SV_HOMEFS}" ]]; then
            # ex: /home/deadsoul
            svpath="/home${sv##*${_SV_DISTRO}${_SV_HOMEFS}}"
        else
            svpath="${sv}"
        fi

        case $svpath in
            '/root' | '/var/log')
                ;;
            *)
                ((checks++))
                #if lsof +D "$svpath" 2>/dev/null | grep "$svpath" >/dev/null 2>&1; then
                if [ $(lsof -w +D "$svpath" 2>/dev/null | awk 'NR>1' | grep "$svpath" 2>/dev/null | grep -v 'snapora' | wc -l) -gt 0 ]; then
                    _log 1 "Checking the state of: '$svpath'. (is busy. try: lsof +D $svpath)"
                    ((fails++))
                    ret=1
                else
                    _log 0 "Checking the state of: '$svpath'. (probably is fine)"
                    ((successes++))
                fi
                ;;
        esac

    done

    if [ "${ret}" == 0 ]; then return 0;
    else return 1; fi
}
#}}}

# does the file has all snapper config variables
function has_all_snapper_vars() { #{{{
    # $1 : file we want to check
    local snapper_vars=(SUBVOLUME FSTYPE QGROUP SPACE_LIMIT FREE_LIMIT ALLOW_USERS ALLOW_GROUPS \
        SYNC_ACL BACKGROUND_COMPARISON NUMBER_CLEANUP NUMBER_MIN_AGE NUMBER_LIMIT NUMBER_LIMIT_IMPORTANT \
        TIMELINE_CREATE TIMELINE_CLEANUP TIMELINE_MIN_AGE TIMELINE_LIMIT_HOURLY TIMELINE_LIMIT_DAILY \
        TIMELINE_LIMIT_WEEKLY TIMELINE_LIMIT_MONTHLY TIMELINE_LIMIT_YEARLY EMPTY_PRE_POST_CLEANUP EMPTY_PRE_POST_MIN_AGE)

    for var in "${snapper_vars[@]}"; do
        if ! grep "$var" "$1" >/dev/null 2>&1; then return 1; fi
    done
    return 0
}
#}}}

# guess the default snapper config template
function guess_snapper_template() { #{{{
    mapfile -t snapper_templates < <(find /etc /usr -type d -name 'snapper' -exec find '{}' -type d -name 'config-templates' -print \; 2>/dev/null)
    local ret=1
    for tpl in "${snapper_templates[@]}"; do
        found_config_template="${tpl}/default"
        # if is file and has contents (size greater than 0)
        if [[ -f "${found_config_template}" && -s "${found_config_template}" ]]; then
            # if it has all snapper config variables
            if has_all_snapper_vars "${found_config_template}"; then
                GUESSED_SNAPPER_CONFIG_TEMPLATES+=("${found_config_template}")
                ret=0
            fi
        fi
    done
    if [ "${ret}" == 0 ]; then return 0;
    else return 1; fi
}
#}}}

# check the snapper default config template
function check_snapper_default_template() { #{{{
    local ret=0
    local suggestion=''
    if guess_snapper_template; then
        suggestion="[Could it be: ${GUESSED_SNAPPER_CONFIG_TEMPLATES[0]}]"
    fi

    if [ ! -z "${_SNAPPER_TEMPLATE}" ]; then
        if [ -f "${_SNAPPER_TEMPLATE}" ]; then
            if has_all_snapper_vars "${_SNAPPER_TEMPLATE}"; then
                _log 0 "Checking: \$_SNAPPER_TEMPLATE='$_SNAPPER_TEMPLATE'. (seems fine)"
            else
                _log 1 "Checking: \$_SNAPPER_TEMPLATE='$_SNAPPER_TEMPLATE'. (is invalid) $suggestion"
                ret=1
            fi
        else
            _log 1 "Checking: \$_SNAPPER_TEMPLATE='$_SNAPPER_TEMPLATE'. (doesn't exist) $suggestion"
            ret=1
        fi
    else
        _log 1 "Checking: \$_SNAPPER_TEMPLATE='$_SNAPPER_TEMPLATE'. (is empty) $suggestion"
        ret=1
    fi

    ((checks++))
    if [ "${ret}" == 0 ]; then
        ((successes++))
        return 0
    else
        ((fails++))
        return 1
    fi
}
#}}}

# run some checks
function run_checks() { #{{{
    local ret=0
    checks=0
    successes=0
    fails=0

    # Make sure we have the root powers
    ((checks++))
    if [ $UID -eq 0 ]; then
        _log 0 "Executed with root privileges."
        ((successes++))
    else
        _log 1 "Executed with root privileges."
        ((fails++))
        ret=1
    fi

    # Check whether or not the distro is Fedora
    ((checks++))
    OSRELEASE=/etc/os-release
    DISTRO=$(grep ^ID= $OSRELEASE 2>/dev/null | cut -d'=' -f2 2>/dev/null)
    if [[ -f $OSRELEASE && $DISTRO == 'fedora' ]]; then
        _log 0 "We're on Fedora."
        ((successes++))
    else
        _log 1 "We're on Fedora. (running script may fail)"
        ((fails++))
        ret=1
    fi

    # Check the existence of the required packages
    if ! check_packages_existence "${REQUIRED_PACKAGES[@]}"; then ret=1; fi

    # Check partitions
    if ! check_partition "$_UEFI_PARTITION" '$_UEFI_PARTITION'; then ret=1; fi
    if ! check_partition "$_BOOT_PARTITION" '$_BOOT_PARTITION'; then ret=1; fi
    if ! check_partition "$_BTRFS_PARTITION" '$_BTRFS_PARTITION'; then ret=1; fi

    # Check btrfs mountpoint
    if ! check_btrfs_mountpoint; then ret=1; fi

    # Check the snapper default config template
    if ! check_snapper_default_template; then ret=1; fi

    # check the state of the directories we need to work with
    if ! check_dir2sv_states; then ret=1; fi

    echo '----------------------------------------------------------------------'
    if [ "${ret}" == 0 ]; then
        _log 0 "Overall checks. (checks: $checks, successes: $successes, fails: $fails. so it's probably fine)\n"
        return 0
    else
        _log 1 "Overall checks. (checks: $checks, successes: $successes, fails: $fails)\n"
        return 1
    fi
}
#}}}
#}}}

# --------------------------------------------------------------------------------

# Handling arguments {{{
case "$1" in
    guess-partitions)
        echo "_UEFI_PARTITION='$(guess_partition efi)'"
        echo "_BOOT_PARTITION='$(guess_partition boot)'"
        echo "_BTRFS_PARTITION='$(guess_partition rootfs)'"
        exit 0
        ;;
    guess-snapper-template)
        if guess_snapper_template; then
            if [ ${#GUESSED_SNAPPER_CONFIG_TEMPLATES[@]} -eq 1 ]; then
                echo "Found ${#GUESSED_SNAPPER_CONFIG_TEMPLATES[@]} snapper config template."
            else
                echo "Found ${#GUESSED_SNAPPER_CONFIG_TEMPLATES[@]} snapper config templates."
            fi
            for snapper_config_template in "${GUESSED_SNAPPER_CONFIG_TEMPLATES[@]}"; do
                echo "${snapper_config_template}"
            done
        else
            echo "Couldn't find any snapper default config template."
            echo "Are you sure, 'snapper' package is installed?"
        fi
        exit 0
        ;;
    run-checks)
        if run_checks; then exit 0;
        else exit 1; fi
        ;;
    *)
        PASSPHRASE='I know this is a usable beta script. So, I took a backup!'
        if [[ $1 != "${PASSPHRASE}" ]]; then
            echo ''
            echo 'This script is not ready to be used yet!!'
            echo ''
            echo 'If you still want to run it, although it might break your system, run it as:'
            echo "${0} '${PASSPHRASE}'"
            echo ''
            echo 'If you would like to contribute, please visit:'
            echo 'https://gitlab.com/DeaDSouL/snapora'
            exit 1
        fi
        ;;
esac
# }}}

run_checks || exit 1

# --------------------------------------------------------------------------------

# Mounting the main partition "$_BTRFS_PARTITION" #{{{
prepare_btrfs_mountpoint

# @TODO: check this
# make sure "${_BTRFS_MNT}" AND|OR "${_BTRFS_PARTITION}" are not being mounted yet, otherwise umount it|them first.
if mount | grep -vE "on (/ |/home )" | grep -E "^${_BTRFS_PARTITION} on.*type btrfs \(.*(subvol=/[,|\)]|subvolid=5[,|\)])" >/dev/null 2>&1; then
    _mountpoint=$(mount | grep -vE "on (/ |/home )" | grep -E "^${_BTRFS_PARTITION} on.*type btrfs \(.*(subvol=/[,|\)]|subvolid=5[,|\)])" | awk '{print $3}')
    umount -v "${_mountpoint}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Un-mounting '${_mountpoint}'." "Couldn't un-mount '${_mountpoint}'!"
fi
if mount | grep "${_BTRFS_MNT}" >/dev/null 2>&1; then
    umount -v "${_BTRFS_MNT}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Un-mounting '${_BTRFS_MNT}'." "Couldn't un-mount '${_BTRFS_MNT}'!"
fi

# mount the top level of btrfs partition
mount -v -o defaults,ssd,noatime,compress=zstd:1,subvolid=5,subvol=/ "${_BTRFS_PARTITION}" "${_BTRFS_MNT}" &>>"${_COMMANDS_OUTPUT}"
_log $? "Mounting the BtrFS rootlevel of: '${_BTRFS_PARTITION}' to: '${_BTRFS_MNT}'" "Couldn't mount '${_BTRFS_MNT}'!"
#}}}

# --------------------------------------------------------------------------------

# Creating the needed subvolumes #{{{
[ ! -z ${_SV_DISTRO} ] && _mksv "${_BTRFS_MNT}/${_SV_DISTRO}"

_mksv "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_SNAPSHOTS}"

# Re-naming subvolume: root to @fedora/@rootfs [OR] @rootfs (depends on the value of ${_SV_DISTRO}
if [ ! -z "${_SV_ROOTFS}" -a "${_SV_ROOTFS}" != "${_SV_ROOTFS_DEFAULT}" ]; then
    _mvsv "${_BTRFS_MNT}/${_SV_ROOTFS_DEFAULT}" "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}"
fi

# Re-naming subvolume: home to @fedora/@homefs [OR] @homefs (depends on the value of ${_SV_DISTRO}
if [ ! -z "${_SV_HOMEFS}" -a "${_SV_HOMEFS}" != "${_SV_HOMEFS_DEFAULT}" ]; then
    _mvsv "${_BTRFS_MNT}/${_SV_HOMEFS_DEFAULT}" "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_HOMEFS}"
fi

# Create subvolumes & move old files
for _dir2sv in "${_DIRS2SV[@]}"; do
    # ex: @fedora/rootfs/var/opt
    _svname="${_dir2sv##*${_BTRFS_MNT}/}"
    if [[ "${_dir2sv}" =~ .*"${_SV_ROOTFS}" ]]; then
        _svpath="${_dir2sv##*${_SV_DISTRO}${_SV_ROOTFS}}"
    elif [[ "${_dir2sv}" =~ "${_SV_HOMEFS}" ]]; then
        _svpath="/home${_dir2sv##*${_SV_DISTRO}${_SV_HOMEFS}}"
    else
        _svpath="${_dir2sv}"
    fi

    # what if it exists but it isn't a directory?!
    _reserve_dirname "${_dir2sv}"

    # if it is a directory
    if [ -e "${_dir2sv}" ]; then
        # if that directory is not a subvolume
        if ! btrfs subvolume list "${_BTRFS_MNT}" | grep "${_svname}\$" > /dev/null 2>&1; then

            # if we're working with '/tmp', we should un-mount it first
            if [ "${_svpath}" == '/tmp' ]; then umount "${_svpath}"; fi

            # rename the old directory
            mv -v "${_dir2sv}"{,.def} &>>"${_COMMANDS_OUTPUT}"
            _log $? "Renaming '${_dir2sv}' to '${_dir2sv}.def'." "Could not rename '${_dir2sv}' to '${_dir2sv}.def'!"

            # create the btrfs subvolume with that name
            _mksv "${_dir2sv}"

            # move everything inside that directory, to the btrfs subvolume
            if [ $(find "${_dir2sv}.def" -mindepth 1 -maxdepth 1 -not -iname '.*'| wc -l) -gt 0 ]; then
                mv -v "${_dir2sv}.def/"* "${_dir2sv}/" &>>"${_COMMANDS_OUTPUT}"
                _log $? "Moving contents of '${_dir2sv}.def' to '${_dir2sv}'." "Could not move contents of '${_dir2sv}.def' to '${_dir2sv}'!"
            fi
            if [ $(find "${_dir2sv}.def" -mindepth 1 -maxdepth 1 -iname '.*'| wc -l) -gt 0 ]; then
                mv -v "${_dir2sv}.def/".[!.]* "${_dir2sv}/" &>>"${_COMMANDS_OUTPUT}"
                _log $? "Moving dotfile contents of '${_dir2sv}.def' to '${_dir2sv}'." "Could not move dotfile contents of '${_dir2sv}.def' to '${_dir2sv}'!"
            fi

            # restore ownership & group
            chown -v --reference="${_dir2sv}.def" "${_dir2sv}" &>>"${_COMMANDS_OUTPUT}"
            _log $? "Restoring the ownership of '${_dir2sv}'." "Could not restore the ownership of  '${_dir2sv}'!"

            # restore octal permission
            chmod -v $(stat -c "%a" "${_dir2sv}.def") "${_dir2sv}" &>>"${_COMMANDS_OUTPUT}"
            _log $? "Restoring the octal permission of '${_dir2sv}'." "Could not restore the octal permission of '${_dir2sv}'!"

            # Make sure we restore the correct security context (SELinux)
            chcon -v --reference="${_dir2sv}.def" "${_dir2sv}" &>>"${_COMMANDS_OUTPUT}"
            _log $? "Restoring the security context (SELinux) of '${_dir2sv}'." "Could not restore the security context (SELinux) of '${_dir2sv}'!"

            # maybe it's better to remove the old empty directory, to keep it clean?
            # rmdir -v "${_dir2sv}.def" &>>"${_COMMANDS_OUTPUT}"
        fi
    else # if we're working with directories their packages aren't installed yet
        if [[ "${_dir2sv}" =~ .*"${_BTRFS_MNT}/${_SV_DISTRO}${_SV_SNAPSHOTS}".* ]]; then
            # if it is a snapshot subvolume, just create the subvolume
            _mksv "${_dir2sv}"
        else # if we're working with directories their packages haven't been installed yet
            _mksv_wpdirs "${_dir2sv}" "${_svpath}"
        fi
    fi
done
#}}}

# Creating the needed .snapshots directories #{{{
for _dir in "" "/root" "/usr/local" $(for _user in "${_HOME_USERS[@]}"; do echo "/home/${_user}"; done); do
    _dirpath="${_dir}/.snapshots"
    if [ -e "${_dirpath}" ]; then
        mv -v "${_dirpath}"{,.def} &>>"${_COMMANDS_OUTPUT}"
        _log $? "Renaming '${_dirpath}' to '${_dirpath}.def'." "Could not rename '${_dirpath}' to '${_dirpath}.def'!"
    else
        mkdir -vp "${_dirpath}" &>>"${_COMMANDS_OUTPUT}"
        _log $? "Creating directory: '${_dirpath}'." "Could not create directory: '${_dirpath}'!"
    fi
done
#}}}

# --------------------------------------------------------------------------------

# Working with /etc/fstab #{{{
# Setting up some variables #{{{
_BTRFS_UUID=$(blkid "${_BTRFS_PARTITION}" -s UUID -o value)
# 0: UUID, 1: TYPE
_UEFI_INFO=($(blkid "${_UEFI_PARTITION}" -s UUID -s TYPE -o value))
_BOOT_INFO=($(blkid "${_BOOT_PARTITION}" -s UUID -s TYPE -o value))
_ROOTFS_ID=$(btrfs subvolume list ${_BTRFS_MNT} | grep "${_SV_DISTRO}${_SV_ROOTFS}\$" | awk '{print $2}')

# needs to be declared first
_FSTAB_MISC=''
_FSTAB_USER=''

# make sure "$_MNT_OPTS" ends with comma ','
if [ ! -z "$_MNT_OPTS" ]; then
    if [ "${_MNT_OPTS: -1}" != ',' ]; then
        _MNT_OPTS="${_MNT_OPTS},"
    fi
fi

# is it disk or partition?
if [ $(lsblk -no TYPE "${_BTRFS_PARTITION}") == "part" ]; then
    _BTRFS_DISK=$(find /sys/devices/ -iname "$(basename ${_BTRFS_PARTITION})" 2>/dev/null | xargs dirname | xargs basename)
    _exit_code=0
elif [ $(lsblk -no TYPE "${_BTRFS_PARTITION}") == "disk" ]; then
    _BTRFS_DISK=$(basename "${_BTRFS_PARTITION}")
    _exit_code=0
else
    _exit_code=1
fi
_log ${_exit_code} "Make sure the '${_BTRFS_PARTITION}' is a disk or partition." "The '${_BTRFS_DISK}' is not a disk nor partition!"

# is our disk an SSD?
if [ -e "/sys/block/${_BTRFS_DISK}/queue/rotational" ]; then
    [ $(cat "/sys/block/${_BTRFS_DISK}/queue/rotational") -eq 0 ] && _MNT_OPTS="${_MNT_OPTS}ssd,"
    _exit_code=0
else
    _exit_code=1
fi
_log ${_exit_code} "Identifying the type of '/dev/${_BTRFS_DISK}'." "Could not identify the type of '/dev/${_BTRFS_DISK}'!"
#}}}

# backing-up default fstab #{{{
_bkp_file "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/fstab"
#}}}

# Preparing fstab mount entries #{{{
_FSTAB_MAIN="
UUID=${_BTRFS_UUID} / btrfs ${_MNT_OPTS}subvol=${_SV_DISTRO}${_SV_ROOTFS} 0 0\n
UUID=${_BTRFS_UUID} /home btrfs ${_MNT_OPTS}subvol=${_SV_DISTRO}${_SV_HOMEFS} 0 0"

_FSTAB_BOOT="
UUID=${_BOOT_INFO[0]} /boot ${_BOOT_INFO[1]} defaults 1 2\n
UUID=${_UEFI_INFO[0]} /boot/efi ${_UEFI_INFO[1]} umask=0077,shortname=winnt 0 2"

for _dir2sv in "${_DIRS2SV[@]}"; do
    # ex: @fedora/rootfs/var/opt
    _svname="${_dir2sv##*${_BTRFS_MNT}/}"
    if [[ "${_dir2sv}" =~ .*"${_SV_ROOTFS}" ]]; then
        _svpath="${_dir2sv##*${_SV_DISTRO}${_SV_ROOTFS}}"
    elif [[ "${_dir2sv}" =~ "${_SV_HOMEFS}" ]]; then
        _svpath="/home${_dir2sv##*${_SV_DISTRO}${_SV_HOMEFS}}"
    else
        _svpath="${_dir2sv}"
    fi

    # Build the extra subvolumes mount entries for '/etc/fstab' file
    if [[ "${_dir2sv}" =~ .*"${_SV_SNAPSHOTS}".* ]]; then
        :
    elif [[ "${_dir2sv}" =~ .*"${_SV_HOMEFS}".* ]]; then
        if _sv_needs_cow "${_svpath}"; then
            _FSTAB_USER="${_FSTAB_USER}\nUUID=${_BTRFS_UUID} ${_svpath} btrfs ${_MNT_OPTS}nodatacow,subvol=${_svname} 0 0\n"
        else
            _FSTAB_USER="${_FSTAB_USER}\nUUID=${_BTRFS_UUID} ${_svpath} btrfs ${_MNT_OPTS}subvol=${_svname} 0 0\n"
        fi
    else
        if _sv_needs_cow "${_svpath}"; then
            _FSTAB_MISC="${_FSTAB_MISC}\nUUID=${_BTRFS_UUID} ${_svpath} btrfs ${_MNT_OPTS}nodatacow,subvol=${_svname} 0 0\n"
        else
            _FSTAB_MISC="${_FSTAB_MISC}\nUUID=${_BTRFS_UUID} ${_svpath} btrfs ${_MNT_OPTS}subvol=${_svname} 0 0\n"
        fi
    fi
done

_FSTAB_SNAP="
UUID=${_BTRFS_UUID} /.snapshots btrfs ${_MNT_OPTS}subvol=${_SV_DISTRO}${_SV_SNAPSHOTS}/@rootfs 0 0\n
UUID=${_BTRFS_UUID} /root/.snapshots btrfs ${_MNT_OPTS}subvol=${_SV_DISTRO}${_SV_SNAPSHOTS}/@home-root 0 0\n
UUID=${_BTRFS_UUID} /usr/local/.snapshots btrfs ${_MNT_OPTS}subvol=${_SV_DISTRO}${_SV_SNAPSHOTS}/@usr-local 0 0"
for _user in "${_HOME_USERS[@]}"; do
        _FSTAB_SNAP="${_FSTAB_SNAP}\nUUID=${_BTRFS_UUID} /home/${_user}/.snapshots btrfs ${_MNT_OPTS}subvol=${_SV_DISTRO}${_SV_SNAPSHOTS}/@home-${_user} 0 0\n"
done
#}}}

# Generating new /etc/fstab #{{{
#cat >"${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/fstab"<<EOL
#cat <<EOL >"${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/fstab"
cat >"${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/fstab"<<EOL
#
# /etc/fstab
# Created by Snapora on ${_FSTAB_DT}
#
# The old /etc/fstab has been renamed to /etc/fstab.bkp-${_DATETIME}
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#

# @rootfs & @homefs
$(echo -e ${_FSTAB_MAIN} | column -t)

# BOOT & EFI
$(echo -e ${_FSTAB_BOOT} | column -t)

# Users Subvolumes
$(echo -e ${_FSTAB_USER} | sort -h | column -t)

# Extra Subvolumes
# do we really need those extra options??       autodefrag,commit=120
$(echo -e ${_FSTAB_MISC} | sort -h | column -t)

# Snapshots
$(echo -e ${_FSTAB_SNAP} | column -t)

EOL
_log $? "Generating '${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/fstab'." "Could not generate '${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/fstab'!"
#}}}
#}}}

# --------------------------------------------------------------------------------

# Working with Snapper #{{{
# Generating snapper configs #{{{
_enable_configs=''
for _sv_path in "${!_SNAP_CONFIGS[@]}"; do
    _snapper_config="/etc/snapper/configs/${_SNAP_CONFIGS[${_sv_path}]}"

    # if it already exists, rename it
    if [ -e "${_snapper_config}" ]; then
        mv -v "${_snapper_config}"{,.bkp-${_DATETIME}} &>>"${_COMMANDS_OUTPUT}"
        _log $? "Renaming the old snapper config: '${_snapper_config}'." "Could not rename '${_snapper_config}'!"
    fi

    # Adding the snapper config

    # Locate snapper/config-templates/default
    if [[ ! -f $_SNAPPER_TEMPLATE || -z $_SNAPPER_TEMPLATE ]]; then
        mapfile -t _snapper_default_template_dir < <(find /etc /usr -type d -name 'snapper' -exec find '{}' -type d -name 'config-templates' -print \; 2>/dev/null)
        if [ -z "${_snapper_default_template_dir}" ]; then
            # f37-
            _snapper_default_template_dir="/etc/snapper/config-templates"
            # f38+
            _snapper_default_template_dir="/usr/share/snapper/config-templates"
        fi
        _SNAPPER_TEMPLATE="${_snapper_default_template_dir}/default"
    fi


    cp -Zapv "${_SNAPPER_TEMPLATE}" "${_snapper_config}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Copying: '${_SNAPPER_TEMPLATE}' to: '${_snapper_config}'." "Could not copy: '${_SNAPPER_TEMPLATE}' to: '${_snapper_config}'!"

    # Make sure we have the correct octal permission
    chmod -v 640 "${_snapper_config}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Restoring the octal permission of '${_snapper_config}'." "Could not restore the octal permission of '${_snapper_config}'!"

    # Make sure we restore the correct security context (SELinux)
    chcon -v --reference="${_SNAPPER_TEMPLATE}" "${_snapper_config}" &>>"${_COMMANDS_OUTPUT}"
    _log $? "Restoring the security context (SELinux) of '${_snapper_config}'." "Could not restore the security context (SELinux) of '${_snapper_config}'!"

    # Adding the correct subvolume path
    sed -i 's%SUBVOLUME=".*"%SUBVOLUME="'${_sv_path}'"%' "${_snapper_config}"
    _log $? "Adding '${_sv_path}' as the subvolume path to '${_snapper_config}'." "Could not add '${_sv_path}' as the subvolume path to '${_snapper_config}'!"

    # Adding the common Snapper settings
    _chfile_values _SNAP_SETTINGS_BOTH "${_snapper_config}"
    _log $? "Adding the common Snapper settings to '${_snapper_config}'." "Could not add the common Snapper settings to '${_snapper_config}'!"

    # Adding the specific Snapper settings
    if [[ "${_SNAP_CONFIGS[${_sv_path}]}" == 'root' ]]; then
        _chfile_values _SNAP_SETTINGS_SYS "${_snapper_config}"
        _log $? "Adding root specific Snapper settings to '${_snapper_config}'." "Could not add root specific Snapper settings to '${_snapper_config}'!"
    elif [[ " ${_sv_path} " =~ ' /home/'.* ]]; then
        sed -i 's%ALLOW_USERS=".*"%ALLOW_USERS="'$(basename ${_sv_path})'"%' "${_snapper_config}"
        _chfile_values _SNAP_SETTINGS_USER "${_snapper_config}"
        _log $? "Adding user specific Snapper settings to '${_snapper_config}'." "Could not add user specific Snapper settings to '${_snapper_config}'!"
    else #ex: /usr/local and /root
        _chfile_values _SNAP_SETTINGS_USER "${_snapper_config}"
        _ecode=$?
        sed -i 's%SYNC_ACL=".*"%SYNC_ACL="no"%' "${_snapper_config}"
        [ $? != $_ecode ] && _ecode=1
        _log $_ecode "Adding specific Snapper settings to '${_snapper_config}'." "Could not add specific Snapper settings to '${_snapper_config}'!"
    fi

    # build the added snapper configs variable
    _enable_configs="${_enable_configs}${_SNAP_CONFIGS[${_sv_path}]} "
done
#}}}

# enable the added snapper configs #{{{
if [ "${_enable_configs: -1}" == ' ' ]; then
    _enable_configs=${_enable_configs::-1}
fi
sed -i 's%SNAPPER_CONFIGS=".*"%SNAPPER_CONFIGS="'"${_enable_configs}"'"%' "/etc/sysconfig/snapper"
_log $? "Enabling the added Snapper configs in '/etc/sysconfig/snapper'." "Could not enable the added Snapper configs in '/etc/sysconfig/snapper'!"
#}}}
#}}}

# --------------------------------------------------------------------------------

# Updating Grub #{{{
grubby --update-kernel=ALL --remove-args="rootflags=subvol=${_SV_ROOTFS_DEFAULT}" &>>"${_COMMANDS_OUTPUT}"
_log $? "Removing 'rootflags=subvol=${_SV_ROOTFS_DEFAULT}' flag in GRUB."

btrfs subvolume set-default "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}"
_log $? "Setting BtrFS default subvolume to '${_SV_DISTRO}${_SV_ROOTFS}'."
#}}}

# --------------------------------------------------------------------------------

# updatedb.conf tweak #{{{
# To avoid potential slowdowns, exclude any .snapshots subvolumes in updatedb.conf(5) so that they aren’t indexed by mlocate(1).
if ! grep 'PRUNENAMES.*\.snapshotss' "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/updatedb.conf" > /dev/null 2>&1; then
        # Adding '.snapshots' to $PRUNENAMES in /etc/updatedb.conf
        _PRUNENAMES=$(sed -n 's/.*PRUNENAMES = \"\([^\"]*\)\".*/\1/p' "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/updatedb.conf")
        sed -i "s/PRUNENAMES.*=.*\".*\"/PRUNENAMES = \".snapshots ${_PRUNENAMES}\"/" "${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/updatedb.conf" &>>"${_COMMANDS_OUTPUT}"
        # We don't have to exit if we didn't succeed in previous command
        _log $? "Adding '.snapshots' to '${_BTRFS_MNT}/${_SV_DISTRO}${_SV_ROOTFS}/etc/updatedb.conf'."
fi
#}}}

# --------------------------------------------------------------------------------

touch /.autorelabel
_cleanup
echo "The system needs to be rebooted!"
echo "It will boot in a special mode (the auto relable mode)!"

_exit 0
