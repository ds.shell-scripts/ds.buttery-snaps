# Snapora
`Snapora` is a BASH script that prepares your Fedora (which uses BtrFS) to be able to use Snapper properly (for taking snapshots and being able to roll them back). With a complete separation between system's side and the user's side.
It's meant to be used once.
![/etc/fstab](https://gitlab.com/DeaDSouL/Snapora/-/raw/main/generated_fstab.png)


## IMPORTANT WARNING
* ~~DO NOT USE IT. Please note it's not ready to be used yet. As it's still under development. Once it's ready, this warning shall be removed.~~
* Make sure you take backup of your system before running the `Snapora`.
* `Snapora` reached a testing state. Which means it's not stable yet, although it's usable now. That does not mean you don't need to backup your system before using it.
* Make sure not to invoke anything that uses `/tmp` like `tmux`, since the script will try to un-mount and rename it, in order to create a subvolume with the same name.
* Because of the previous point, the script should be run in the `rescue.target` mode.


## Description
#### What is Snapora?
Since [Fedora](https://getfedora.org/) 33, and [BtrFS](https://en.wikipedia.org/wiki/Btrfs) is the default filesystem. Yet they don't make use of the BtrFS snapshots nor they provide a tool to make this an easy operation by default. That's why this script is born. To save you a lot of time, and automate all the things that need to be done, in order to be able to take proper snapshots and be able as well to roll them back whenever it's needed. Whether they were system or user snapshots.


## Requirements
`Snapora` needs `grubby`, `snapper`, `python3-dnf-plugin-snapper` and `pam_snapper`. To install them:
```
su -
dnf install grubby snapper python3-dnf-plugin-snapper pam_snapper
```


## Installation
First, clone the repository:
```
cd /usr/local/src
git clone https://gitlab.com/DeaDSouL/snapora.git
```

Second, take a backup. I'M SERIOUS, TAKE A BACKUP!!

Then, edit `snapora.sh`'s settings:
```
_UEFI_PARTITION='/dev/nvme0n1p1'
_BOOT_PARTITION='/dev/nvme0n1p2'
_BTRFS_PARTITION='/dev/nvme0n1p3'
```

You can ask the script to guess the correct values by running:
```
bash snapora.sh guess-partitions
```

You also need to modify the following:
```
_snapper_default_template='/usr/share/snapper/config-templates/default'
```

You can ask the script to guess the correct default snapper config template path, by running:
```
bash snapora.sh guess-snapper-template
```

Again, take a backup. for your own sake.

Finally, reboot the system:
```
reboot
```

Once you're in `GRUB` menu, select the default option, and hit `e`.

After that, append the flag: `systemd.unit=rescue.target`. right after the word: `quiet`.

Then, hit: `Ctrl`+`x`.

Type `root`'s password, when you're asked to, 

Once you're logged-in.. type:
```
cd /usr/local/src
bash snapora/snapora.sh 'I know this is a usable beta script. So, I took a backup!'
```
When the script has done its thing, just `reboot`. And that's it!!


## Usage
Guess the correct partitions:
```
bash snapora.sh guess-partitions
```

Guess the correct default snapper config template path:
```
bash snapora.sh guess-snapper-template
```

Run some checks, to determine whether or not the script will fail:
```
bash snapora.sh run-checks
```

Run the script:
```
bash snapora.sh 'I know this is a usable beta script. So, I took a backup!'
```


## Support
For now you can use this repository's [issues](https://gitlab.com/DeaDSouL/snapora/-/issues) page.


## Limitation
* Does not support the encrypted disks.
* Does not support partition-less disks.
* Does not support swap mount entry in `/etc/fstab`.


## Roadmap
* Support the encrypted disks.
* Support partition-less disks.
* Add revert (undo) option.
* Support swap mount entry in `/etc/fstab`.


## Contributing
You're more than welcome to contribute if you want to. Since this script will serve us all.


## Authors and acknowledgment
Who wants to be listed here ;).


## Project status
Active and under development as long as Fedora doesn't implement something that achieves the purpose of this script.

## FAQs:

### Snapora, explain the name!
The *Snap* in *Sna*pora, came from *Snap*shot which is one of the `BtrFS` main features, as well as `Snapper` since it relies on it to take system snapshots when `DNF` does any changes (install / remove / upgrade).
While the *ora* came from Fed*ora* since `Snapora` was written specially for `Fedora`.

Note: It used to be called `ds.buttery-snaps`, what a name huh? :p)

### Ok, What does Snapora do exactly?
It will:
* Create the needed subvolumes.
* Generate a new `/etc/fstab` file.
* Install `Snapper` and create config files for:
  * `/`.
  * `/home/[USERS]`.
  * `/root`.
  * `/usr/local`.
* Avoid potential slowdowns caused by `.snapshots` directories.
* Integrate `Snapper` with `GRUB` entries, for better rollbacks.

### What subvolumes are going to be created by default?
It will first create a main subvolume called `@fedora`. Then it will rename and move the `root` & `home` subvolumes to `@fedora/@rootfs` and `@fedora/@homefs` respectively.

After that the following subvolumes are going to be created:
```
@fedora
├── @homefs <-- (mounted as: /home)
│   ├── user1
│   │   ├── .cache
│   │   ├── .local <--------------- (is a directory)
│   │   │   └── share <------------ (is a directory)
│   │   │       ├── containers
│   │   │       └── gnome-boxes <-- (is a directory)
│   │   │           └── images <--- (mounted with option: nodatacow)
│   │   ├── .var
│   │   └── Downloads
│   ├── user2
│   │   └── ((Same as above.))
│   └── userN
│       └── ((Same as above.))
├── @rootfs <-- (mounted as: /)
│   ├── opt
│   ├── root
│   ├── srv
│   ├── tmp
│   ├── usr <----------------- (is a directory)
│   │   └── local
│   └── var <----------------- (is a directory)
│       ├── cache
│       ├── crash
│       ├── lib <------------- (is a directory)
│       │   ├── containers
│       │   ├── libvirt <----- (is a directory)
│       │   │   └── images <-- (mounted with option: nodatacow)
│       │   ├── mariadb <----- (mounted with option: nodatacow)
│       │   ├── mysql <------- (mounted with option: nodatacow)
│       │   └── pgqsl <------- (mounted with option: nodatacow)
│       ├── log
│       ├── opt
│       ├── spool
│       └── tmp
└── @snapshots
    ├── @home-root <--- (mounted as: /root/.snapshots)
    ├── @home-user1 <-- (mounted as: /home/user1/.snapshots)
    ├── @home-user2 <-- (mounted as: /home/user2/.snapshots)
    ├── @home-userN <-- (mounted as: /home/userN/.snapshots)
    ├── @rootfs <------ (mounted as: /.snapshots)
    └── @usr-local <--- (mounted as: /usr/local/.snapshots)
```

### So many subvolumes. Why??
#### Short answer:
Some directories are just useless to be rolled back, such as `/var/tmp`, `/var/cache`. And some directories should never be rolled back, like `/root`, `/var/log`.

#### Long answer:
* System side:
  * `/root`: The `root` users home directory should also be preserved during a rollback.
  * `/tmp`: All directories containing temporary files and caches are excluded from snapshots.⋅
  * `/run`: Maybe not, since it's being mounted as tmpfs in Fedora (35).
  * `/opt`: Third-party products usually get installed to `/opt`. It is excluded to avoid uninstalling these applications on rollbacks.
  * `/srv`: Contains data for Web and FTP servers. It is excluded to avoid data loss on rollbacks.
  * `/var/log`: Log file location. Excluded from snapshots to allow log file analysis after the rollback of a broken system.
  * `/var/opt`: Third-party products usually get installed to `/opt`. It is excluded to avoid uninstalling these applications on.
  * `/var/tmp`, `/var/crash`, `/var/cache`: All directories containing temporary files and caches are excluded from snapshots.
  * `/var/lib/mailman`, `/var/spool`: Directories containing mails or mail queues are excluded to avoid a loss of mails after a rollback.
  * `/usr/local`: Custom-built scripts or software, to be clearly separated from the distribution software.
  * `/var/lib/libvirt/images`: The default location for virtual machine images managed with libvirt. Excluded to ensure virtual machine images are not replaced with older versions during a rollback. By default, this subvolume is created with the option no copy on write. This directory should have Copy-on-Write disabled when mounting the subvolume with the `nodatacow` mount option. This⋅ Avoids CoW on CoW per the caution in the [Debian Wiki’s page on Btrfs](https://wiki.debian.org/Btrfs).
  * `/var/lib/containers`: This is where Podman stores its containers, so it's given a dedicated subvolume to allow for rollbacks on this directory.
  * `/var/lib/named`: Contains zone data for the DNS server. Excluded from snapshots to ensure a name server can operate after a rollback.
  * `/var/lib/mariadb`, `/var/lib/mysql`, `/var/lib/pgqsl`: These directories contain database data. By default, these subvolumes are created with the option no copy on write.
* User side:
  * `/home`: Is excluded from rootfs snapshots to avoid data loss on rollbacks.
  * `/home/[USER]`: Each user will have and control his/her own snapshots.
  * `/home/[USER]/.cache`: Local cache files don’t need to be included in snapshots, so they aren’t.
  * `/home/[USER]/.local/share/containers`: Non-root Podman containers are stored in a sub-directory called `storage` for a particular user so `/home/[USER]/.local/share/containers` is given a dedicated subvolume in case you want to create snapshots of it at some point in the future. Used to be `/home/[USER]/.local/share/containers/storage`, but since `podman` needs to be able to remove that directory when `podman system reset` is issued, we can't have it to be a subvolume. That's why we're making its parent directory a subvolume.
  * `/home/[USER]/.local/share/gnome-boxes/images`: This subvolume should have CoW disabled as it contains virtual machine disk images for GNOME Boxes.
  * `/home/[USER]/.var`: Per-user Flatpak installations are kept in `.var` and so this entire directory excluded from snapshots. This is documented in the Flatpak documentation [here](https://docs.flatpak.org/en/latest/conventions.html?highlight=.var#xdg-base-directories). Note that the config files for each application might be valuable.

  ##### Too many subvolumes in `/var`:
  Note that we could create only one subvolume for `/var` and mount it with `nodatacow`, just like how [OpenSUSE](https://en.opensuse.org/SDB:BTRFS#Default_Subvolumes) does. but I think the way we do it is much better.

  ##### Resources:
  * [OpenSUSE default subvolumes](https://en.opensuse.org/SDB:BTRFS#Default_Subvolumes).
  * [Debian wiki's BtrFS page](https://wiki.debian.org/Btrfs).
  * [Home Directory section & systemd file-hierarchy(7)](https://www.freedesktop.org/software/systemd/man/file-hierarchy.html)
  * [Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.html)


## License
Although, this program is a free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see [gnu.org/licenses](http://www.gnu.org/licenses/).

Copyright (C) [DeaDSouL](https://gitlab.com/DeaDSouL).
